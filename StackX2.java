package Stack;

public class StackX2 {
    private int maxSize;
    private int top;
    private int[] stackArray;

    public StackX2(int size) {
        maxSize = size;
        stackArray = new int[maxSize];
        top = -1;
    }

    public void push(int value) {
        if (isFull()) {
            System.out.println("Stack เต็ม");
            return;
        }
        stackArray[++top] = value;
    }

    public int pop() {
        if (isEmpty()) {
            System.out.println("Stack ว่าง");
            return -1; // หรือค่าที่เหมาะสมในกรณี Stack ว่าง
        }
        return stackArray[top--];
    }

    public int peek() {
        if (isEmpty()) {
            System.out.println("Stack ว่าง");
            return -1; // หรือค่าที่เหมาะสมในกรณี Stack ว่าง
        }
        return stackArray[top];
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (top == maxSize - 1);
    }

    public int size() {
        return top + 1;
    }

    public static void main(String[] args) {
        StackX2 stack = new StackX2(5);

        stack.push(5);
        stack.push(10);
        stack.push(15);

        System.out.println("ข้อมูลบน Stack: " + stack);

        int topValue = stack.pop();
        System.out.println("ข้อมูลที่ถูกดึง: " + topValue);
        System.out.println("ข้อมูลบน Stack หลังจากดึง: " + stack);

        int peekValue = stack.peek();
        System.out.println("ข้อมูลบน Stack (peek): " + peekValue);

        System.out.println("Stack ว่างหรือไม่: " + stack.isEmpty());
        System.out.println("ขนาดของ Stack: " + stack.size());
    }
}

